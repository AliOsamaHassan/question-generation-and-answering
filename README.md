# Automatic Question Generation

This program takes a text file as an input and generates questions by analyzing each sentence. 

## Usage

**Virtualenv recommended**

`pip install -r requirements.txt`

`python -m textblob.download_corpora` 

*Use `verbose` parameter to activate verbose*

Run the .ipynb fie called `RunMe.ipynb`

*You can also try inputing any text file.*

## How does this work?

**A text file passed as argument to function `generateQuestions`.**

- The text file is read using a Python package called **`textblob`**.
- Each paragraph is further broken down into sentences using the function **`parse(string):`**
- And each sentence is passed as string to function **`genQuestion(line):`**

**These are the part-of-speech tags which is used in this demo.**

```
NNS 	Noun, plural
JJ 	Adjective 
NNP 	Proper noun, singular 
VBG 	Verb, gerund or present participle 
VBN 	Verb, past participle 
VBZ 	Verb, 3rd person singular present 
VBD 	Verb, past tense 
IN 		Preposition or subordinating conjunction 
PRP 	Personal pronoun 
NN 	Noun, singular or mass 
```

**Ref:** [Alphabetical list of part-of-speech tags used in the Penn Treebank Project](http://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html)

**This program uses a small list of combinations.**

```
    l1 = ['NNP', 'VBG', 'VBZ', 'IN']
    l2 = ['NNP', 'VBG', 'VBZ']
    l3 = ['PRP', 'VBG', 'VBZ', 'IN']
    l4 = ['PRP', 'VBG', 'VBZ']
    l5 = ['PRP', 'VBG', 'VBD']
    l6 = ['NNP', 'VBG', 'VBD']
    l7 = ['NN', 'VBG', 'VBZ']
    l8 = ['NNP', 'VBZ', 'JJ']
    l9 = ['NNP', 'VBZ', 'NN']
    l10 = ['NNP', 'VBZ']
    l11 = ['PRP', 'VBZ']
    l12 = ['NNP', 'NN', 'IN']
    l13 = ['NN', 'VBZ']
```

Each sentence is parsed using English grammar rules with the use of condition statements.
A dictionary is created called **`bucket`** and the part-of-speech tags are added to it.

The sentence which gets parsed successfully generates a question sentence. 
The generated question list is printed as output.

**This demo only uses the grammar to generate questions starting with 'what'.**


## Example

**Sentence:**

-----------INPUT TEXT-------------

```
Neuro Hypnotic Healing
Creating a partnership within your consciousness for health and well being

I have been working in the field of Neuro Linguistic Programming and Hypnosis as a Trainer and Coach for over 22 years now.

 

As a Trainer and Coach, it’s great privilege to be part of an individual’s life transformation and I never cease to be amazed at the power and efficacy NLP and hypnosis has for helping people have access to new choices in their lives.

Originally my work focussed on psychological challenges people were experiencing in their lives, and gradually over the years there has been a shift where now a significant portion of my work is supporting people with health issues to regain health and be well. 

The range of health challenges I work with go across the health spectrum including allergies, eyesight issues, hearing problems to more chronic diseases such as cancer, liver conditions, Lyme disease and lupus.

I call this work Neuro Hypnotic Healing as the models I have built are a tapestry of patterns drawing upon the field of Neuro Linguistic Programming (NLP - Grinder and Bandler), New Code NLP (Grinder and Bostic) and Hypnosis. 

  The premise of Neuro Hypnotic Healing is to use hypnotic patterning to connect deep with choices inherent in a client’s unconscious mind to establish healthy choices.



The nervous system acts like a messenger within our body-mind, receiving and processing messages from the outside world and passing the messages on to other systems within the body-mind, e.g. immune system, endocrine system and lymph system. 

Harmony within the systems mentioned, mediated by the messenger (nervous system) will promote good health and well being.

For the purpose of this work we make an illusionary split in different forms of consciousness. There is the conscious mind, which represents our moment to moment explicit attention and the unconscious mind which represents everything in our system we are not conscious of or can consciously influence.

The vast amount of information in the unconscious has hugely different functions. So all elements of the body system are within the unconscious mind, the immune system, digestive system, the endocrine system, nervous system and so on.

The functions and processes of the systems mentioned are out of conscious awareness and influence. Thus meaning you do not have to think about engaging your immune system to combat germs and diseases and you do not have to instruct your lymph system to cleanse your body as these processes happen naturally without thinking. 

In healthy people, the aforementioned systems, when functioning normally are far removed from any thought processes.

When there is a problem in the body, the unconscious mind sends a signal via the nervous system. The signal is frequently experienced as pain or some other physical response.  A simple example is if you ate a very heavy lunch excessively, your digestive system will signal through indigestion that it is struggling to break down the food you have consumed. 

Chemical shifts in our body through stress and also diet, could create an ulcer, that ulcer is the unconscious mind’s way of saying ‘change something’.

The above logic can be followed through to more serious issues such as cancer or heart disease. With cancer the immune system is having a challenge in combating the acceleration of cancerous cells in the body and a tumour is the signal of the shift.  

With heart disease, what we consume is an influence and also how we live in relation to stress which has an enormous influence on our heart.

In 1970s, the co–creators of Neuro Linguistic Programming, Grinder and Bandler created a series of operational assumptions for the practice NLP. 

One of these assumptions was ‘the mind and body are a linked system’. Today most medical practitioners and psychologists would openly accept the mind body connection, however the hypothesis was a lot more radical in 1972. 

In New Code NLP we take the mind body presupposition a stage further and consider the mind and body as part of the same system, with every thought (mind) being expressed in the body and every feeling (body) being expressed in the mind. 

These are not linked systems they are different processes within the same system. This ideology of the mind and body as one body-mind, promotes the rapid healing that occurs within the Neuro Hypnotic Healing processes.

For the last 3 centuries, thinking has been promoted as superior to feeling and sensing. Thinking is unique to human beings, and we can even manage to have diverse conversations with ourselves on conflicting topics, convincing ourselves of a position on a life topic and completely change our minds the following day. 

The expression ‘I think therefore I am’ (Rene Descartes) positioned the logic of thinking (not always logical) over the processes of ‘being’. Being is the processes inherent in the body. Thinking is the key feature of the conscious mind, and the conscious mind seeks to dominate with its thinking.

When a person lets their conscious attention be dominant, it could be said the person is living in their head.  Such head dominance leads to disconnection from body awareness so apart for perhaps the primal needs of going to the toilet, hunger and sex, the person living in their head has little awareness of their body. 

Such dissociation is unhealthy on the basis without body awareness the messages from the unconscious mind signalling a change in the system are not experienced and a health issue develops unnoticed. 

One of the key premises of the Neuro Hypnotic Healing system is to a partnership between the conscious and unconscious mind. In this partnership the conscious mind can take the role of organiser (rather than controller) and the unconscious can take the role of provider, providing the necessary resources for healing and well being. 

This may seem simplistic but such a partnership between the conscious and unconscious puts the mind and body as one is the route for a healthy life.

About The Author
Michael Carroll is the founder and course director of the NLP Academy and co-founder with John Grinder and Carmen Bostic St Clair of the International Trainers Academy of NLP. 

He is the only NLP Master Trainer in the world certified by John Grinder and Carmen Bostic St Clair and he works closely with them in developing and delivering high quality NLP training.

-----------INPUT END---------------
```

**Generated questions.**

```
 Question: What's Trainer helping?

 Question: What has work experiencing?

 Question: What is Neuro?

 Question: What acts system receiving?

 Question: What has amount?

 Question: What is problem?

 Question: What is signal?

 Question: What is you struggling?

 Question: What is Chemical saying?

 Question: What is cancer having?

 Question: What promotes Neuro?

 Question: What has thinking feeling?

 Question: What is we conflicting?

 Question: What is Being?

 Question: What is Thinking?

 Question: What lets it living?

 Question: What leads head going?

 Question: What is dissociation signalling?

 Question: What is Neuro?

 Question: What puts partnership?

 Question: What is Author?

 Question: What is NLP developing?
```

**We can also activate the `verbose` mode by passing True for the `verbose` argument to further understand the question generation process.**

**Output:** with verbose option.

```
Neuro Hypnotic Healing
Creating a partnership within your consciousness for health and well being

I have been working in the field of Neuro Linguistic Programming and Hypnosis as a Trainer and Coach for over 22 years now. 

TAGS: [('Neuro', 'NNP'), ('Hypnotic', 'NNP'), ('Healing', 'NNP'), ('Creating', 'VBG'), ('a', 'DT'), ('partnership', 'NN'), ('within', 'IN'), ('your', 'PRP$'), ('consciousness', 'NN'), ('for', 'IN'), ('health', 'NN'), ('and', 'CC'), ('well', 'RB'), ('being', 'VBG'), ('I', 'PRP'), ('have', 'VBP'), ('been', 'VBN'), ('working', 'VBG'), ('in', 'IN'), ('the', 'DT'), ('field', 'NN'), ('of', 'IN'), ('Neuro', 'NNP'), ('Linguistic', 'NNP'), ('Programming', 'NNP'), ('and', 'CC'), ('Hypnosis', 'NNP'), ('as', 'IN'), ('a', 'DT'), ('Trainer', 'NNP'), ('and', 'CC'), ('Coach', 'NNP'), ('for', 'IN'), ('over', 'IN'), ('22', 'CD'), ('years', 'NNS'), ('now', 'RB')] 

{'NNP': 0, 'VBG': 3, 'DT': 4, 'NN': 5, 'IN': 6, 'PRP$': 7, 'CC': 11, 'RB': 12, 'PRP': 14, 'VBP': 15, 'VBN': 16, 'CD': 34, 'NNS': 35}

 --------------------
As a Trainer and Coach, it’s great privilege to be part of an individual’s life transformation and I never cease to be amazed at the power and efficacy NLP and hypnosis has for helping people have access to new choices in their lives. 

TAGS: [('As', 'IN'), ('a', 'DT'), ('Trainer', 'NNP'), ('and', 'CC'), ('Coach', 'NNP'), ('it', 'PRP'), ('’', 'VBZ'), ('s', 'JJ'), ('great', 'JJ'), ('privilege', 'NN'), ('to', 'TO'), ('be', 'VB'), ('part', 'NN'), ('of', 'IN'), ('an', 'DT'), ('individual', 'JJ'), ('’', 'NN'), ('s', 'JJ'), ('life', 'NN'), ('transformation', 'NN'), ('and', 'CC'), ('I', 'PRP'), ('never', 'RB'), ('cease', 'VBD'), ('to', 'TO'), ('be', 'VB'), ('amazed', 'VBN'), ('at', 'IN'), ('the', 'DT'), ('power', 'NN'), ('and', 'CC'), ('efficacy', 'NN'), ('NLP', 'NNP'), ('and', 'CC'), ('hypnosis', 'NN'), ('has', 'VBZ'), ('for', 'IN'), ('helping', 'VBG'), ('people', 'NNS'), ('have', 'VBP'), ('access', 'NN'), ('to', 'TO'), ('new', 'JJ'), ('choices', 'NNS'), ('in', 'IN'), ('their', 'PRP$'), ('lives', 'NNS')] 

{'IN': 0, 'DT': 1, 'NNP': 2, 'CC': 3, 'PRP': 5, 'VBZ': 6, 'JJ': 7, 'NN': 9, 'TO': 10, 'VB': 11, 'RB': 22, 'VBD': 23, 'VBN': 26, 'VBG': 37, 'NNS': 38, 'VBP': 39, 'PRP$': 45}

 Question: What's Trainer helping?

 --------------------
Originally my work focussed on psychological challenges people were experiencing in their lives, and gradually over the years there has been a shift where now a significant portion of my work is supporting people with health issues to regain health and be well. 

TAGS: [('Originally', 'RB'), ('my', 'PRP$'), ('work', 'NN'), ('focussed', 'VBN'), ('on', 'IN'), ('psychological', 'JJ'), ('challenges', 'NNS'), ('people', 'NNS'), ('were', 'VBD'), ('experiencing', 'VBG'), ('in', 'IN'), ('their', 'PRP$'), ('lives', 'NNS'), ('and', 'CC'), ('gradually', 'RB'), ('over', 'IN'), ('the', 'DT'), ('years', 'NNS'), ('there', 'RB'), ('has', 'VBZ'), ('been', 'VBN'), ('a', 'DT'), ('shift', 'NN'), ('where', 'WRB'), ('now', 'RB'), ('a', 'DT'), ('significant', 'JJ'), ('portion', 'NN'), ('of', 'IN'), ('my', 'PRP$'), ('work', 'NN'), ('is', 'VBZ'), ('supporting', 'VBG'), ('people', 'NNS'), ('with', 'IN'), ('health', 'NN'), ('issues', 'NNS'), ('to', 'TO'), ('regain', 'VB'), ('health', 'NN'), ('and', 'CC'), ('be', 'VB'), ('well', 'RB')] 

{'RB': 0, 'PRP$': 1, 'NN': 2, 'VBN': 3, 'IN': 4, 'JJ': 5, 'NNS': 6, 'VBD': 8, 'VBG': 9, 'CC': 13, 'DT': 16, 'VBZ': 19, 'WRB': 23, 'TO': 37, 'VB': 38}

 Question: What has work experiencing?

 --------------------
The range of health challenges I work with go across the health spectrum including allergies, eyesight issues, hearing problems to more chronic diseases such as cancer, liver conditions, Lyme disease and lupus. 

TAGS: [('The', 'DT'), ('range', 'NN'), ('of', 'IN'), ('health', 'NN'), ('challenges', 'NNS'), ('I', 'PRP'), ('work', 'VBP'), ('with', 'IN'), ('go', 'NNS'), ('across', 'IN'), ('the', 'DT'), ('health', 'NN'), ('spectrum', 'NN'), ('including', 'VBG'), ('allergies', 'NNS'), ('eyesight', 'JJ'), ('issues', 'NNS'), ('hearing', 'VBG'), ('problems', 'NNS'), ('to', 'TO'), ('more', 'RBR'), ('chronic', 'JJ'), ('diseases', 'NNS'), ('such', 'JJ'), ('as', 'IN'), ('cancer', 'NN'), ('liver', 'JJ'), ('conditions', 'NNS'), ('Lyme', 'NNP'), ('disease', 'NN'), ('and', 'CC'), ('lupus', 'NN')] 

{'DT': 0, 'NN': 1, 'IN': 2, 'NNS': 4, 'PRP': 5, 'VBP': 6, 'VBG': 13, 'JJ': 15, 'TO': 19, 'RBR': 20, 'NNP': 28, 'CC': 30}

 --------------------
I call this work Neuro Hypnotic Healing as the models I have built are a tapestry of patterns drawing upon the field of Neuro Linguistic Programming (NLP - Grinder and Bandler), New Code NLP (Grinder and Bostic) and Hypnosis. 

TAGS: [('I', 'PRP'), ('call', 'VBP'), ('this', 'DT'), ('work', 'NN'), ('Neuro', 'NNP'), ('Hypnotic', 'NNP'), ('Healing', 'NNP'), ('as', 'IN'), ('the', 'DT'), ('models', 'NNS'), ('I', 'PRP'), ('have', 'VBP'), ('built', 'VBN'), ('are', 'VBP'), ('a', 'DT'), ('tapestry', 'NN'), ('of', 'IN'), ('patterns', 'NNS'), ('drawing', 'VBG'), ('upon', 'IN'), ('the', 'DT'), ('field', 'NN'), ('of', 'IN'), ('Neuro', 'NNP'), ('Linguistic', 'NNP'), ('Programming', 'NNP'), ('NLP', 'NNP'), ('Grinder', 'NN'), ('and', 'CC'), ('Bandler', 'NNP'), ('New', 'NNP'), ('Code', 'NNP'), ('NLP', 'NNP'), ('Grinder', 'NNP'), ('and', 'CC'), ('Bostic', 'NNP'), ('and', 'CC'), ('Hypnosis', 'NNP')] 

{'PRP': 0, 'VBP': 1, 'DT': 2, 'NN': 3, 'NNP': 4, 'IN': 7, 'NNS': 9, 'VBN': 12, 'VBG': 18, 'CC': 28}

 --------------------
The premise of Neuro Hypnotic Healing is to use hypnotic patterning to connect deep with choices inherent in a client’s unconscious mind to establish healthy choices. 

TAGS: [('The', 'DT'), ('premise', 'NN'), ('of', 'IN'), ('Neuro', 'NNP'), ('Hypnotic', 'NNP'), ('Healing', 'NNP'), ('is', 'VBZ'), ('to', 'TO'), ('use', 'VB'), ('hypnotic', 'JJ'), ('patterning', 'NN'), ('to', 'TO'), ('connect', 'VB'), ('deep', 'JJ'), ('with', 'IN'), ('choices', 'NNS'), ('inherent', 'JJ'), ('in', 'IN'), ('a', 'DT'), ('client', 'NN'), ('’', 'NN'), ('s', 'VBP'), ('unconscious', 'JJ'), ('mind', 'NN'), ('to', 'TO'), ('establish', 'VB'), ('healthy', 'JJ'), ('choices', 'NNS')] 

{'DT': 0, 'NN': 1, 'IN': 2, 'NNP': 3, 'VBZ': 6, 'TO': 7, 'VB': 8, 'JJ': 9, 'NNS': 15, 'VBP': 21}

 Question: What is Neuro?

 --------------------
The nervous system acts like a messenger within our body-mind, receiving and processing messages from the outside world and passing the messages on to other systems within the body-mind, e.g. 

TAGS: [('The', 'DT'), ('nervous', 'JJ'), ('system', 'NN'), ('acts', 'VBZ'), ('like', 'IN'), ('a', 'DT'), ('messenger', 'NN'), ('within', 'IN'), ('our', 'PRP$'), ('body-mind', 'NN'), ('receiving', 'VBG'), ('and', 'CC'), ('processing', 'VBG'), ('messages', 'NNS'), ('from', 'IN'), ('the', 'DT'), ('outside', 'JJ'), ('world', 'NN'), ('and', 'CC'), ('passing', 'VBG'), ('the', 'DT'), ('messages', 'NNS'), ('on', 'IN'), ('to', 'TO'), ('other', 'JJ'), ('systems', 'NNS'), ('within', 'IN'), ('the', 'DT'), ('body-mind', 'NN'), ('e.g', 'NN')] 

{'DT': 0, 'JJ': 1, 'NN': 2, 'VBZ': 3, 'IN': 4, 'PRP$': 8, 'VBG': 10, 'CC': 11, 'NNS': 13, 'TO': 23}

 Question: What acts system receiving?

 --------------------
immune system, endocrine system and lymph system. 

TAGS: [('immune', 'NN'), ('system', 'NN'), ('endocrine', 'NN'), ('system', 'NN'), ('and', 'CC'), ('lymph', 'NN'), ('system', 'NN')] 

{'NN': 0, 'CC': 4}

 --------------------
Harmony within the systems mentioned, mediated by the messenger (nervous system) will promote good health and well being. 

TAGS: [('Harmony', 'NNP'), ('within', 'IN'), ('the', 'DT'), ('systems', 'NNS'), ('mentioned', 'VBN'), ('mediated', 'VBN'), ('by', 'IN'), ('the', 'DT'), ('messenger', 'NN'), ('nervous', 'JJ'), ('system', 'NN'), ('will', 'MD'), ('promote', 'VB'), ('good', 'JJ'), ('health', 'NN'), ('and', 'CC'), ('well', 'RB'), ('being', 'VBG')] 

{'NNP': 0, 'IN': 1, 'DT': 2, 'NNS': 3, 'VBN': 4, 'NN': 8, 'JJ': 9, 'MD': 11, 'VB': 12, 'CC': 15, 'RB': 16, 'VBG': 17}

 --------------------
For the purpose of this work we make an illusionary split in different forms of consciousness. 

TAGS: [('For', 'IN'), ('the', 'DT'), ('purpose', 'NN'), ('of', 'IN'), ('this', 'DT'), ('work', 'NN'), ('we', 'PRP'), ('make', 'VBP'), ('an', 'DT'), ('illusionary', 'JJ'), ('split', 'NN'), ('in', 'IN'), ('different', 'JJ'), ('forms', 'NNS'), ('of', 'IN'), ('consciousness', 'NN')] 

{'IN': 0, 'DT': 1, 'NN': 2, 'PRP': 6, 'VBP': 7, 'JJ': 9, 'NNS': 13}

 --------------------
There is the conscious mind, which represents our moment to moment explicit attention and the unconscious mind which represents everything in our system we are not conscious of or can consciously influence. 

TAGS: [('There', 'EX'), ('is', 'VBZ'), ('the', 'DT'), ('conscious', 'JJ'), ('mind', 'NN'), ('which', 'WDT'), ('represents', 'VBZ'), ('our', 'PRP$'), ('moment', 'NN'), ('to', 'TO'), ('moment', 'NN'), ('explicit', 'JJ'), ('attention', 'NN'), ('and', 'CC'), ('the', 'DT'), ('unconscious', 'JJ'), ('mind', 'NN'), ('which', 'WDT'), ('represents', 'VBZ'), ('everything', 'NN'), ('in', 'IN'), ('our', 'PRP$'), ('system', 'NN'), ('we', 'PRP'), ('are', 'VBP'), ('not', 'RB'), ('conscious', 'JJ'), ('of', 'IN'), ('or', 'CC'), ('can', 'MD'), ('consciously', 'RB'), ('influence', 'VB')] 

{'EX': 0, 'VBZ': 1, 'DT': 2, 'JJ': 3, 'NN': 4, 'WDT': 5, 'PRP$': 7, 'TO': 9, 'CC': 13, 'IN': 20, 'PRP': 23, 'VBP': 24, 'RB': 25, 'MD': 29, 'VB': 31}

 --------------------
The vast amount of information in the unconscious has hugely different functions. 

TAGS: [('The', 'DT'), ('vast', 'JJ'), ('amount', 'NN'), ('of', 'IN'), ('information', 'NN'), ('in', 'IN'), ('the', 'DT'), ('unconscious', 'JJ'), ('has', 'VBZ'), ('hugely', 'RB'), ('different', 'JJ'), ('functions', 'NNS')] 

{'DT': 0, 'JJ': 1, 'NN': 2, 'IN': 3, 'VBZ': 8, 'RB': 9, 'NNS': 11}

 Question: What has amount?

 --------------------
So all elements of the body system are within the unconscious mind, the immune system, digestive system, the endocrine system, nervous system and so on. 

TAGS: [('So', 'RB'), ('all', 'DT'), ('elements', 'NNS'), ('of', 'IN'), ('the', 'DT'), ('body', 'NN'), ('system', 'NN'), ('are', 'VBP'), ('within', 'IN'), ('the', 'DT'), ('unconscious', 'JJ'), ('mind', 'NN'), ('the', 'DT'), ('immune', 'NN'), ('system', 'NN'), ('digestive', 'JJ'), ('system', 'NN'), ('the', 'DT'), ('endocrine', 'NN'), ('system', 'NN'), ('nervous', 'JJ'), ('system', 'NN'), ('and', 'CC'), ('so', 'RB'), ('on', 'IN')] 

{'RB': 0, 'DT': 1, 'NNS': 2, 'IN': 3, 'NN': 5, 'VBP': 7, 'JJ': 10, 'CC': 22}

 --------------------
The functions and processes of the systems mentioned are out of conscious awareness and influence. 

TAGS: [('The', 'DT'), ('functions', 'NNS'), ('and', 'CC'), ('processes', 'NNS'), ('of', 'IN'), ('the', 'DT'), ('systems', 'NNS'), ('mentioned', 'VBN'), ('are', 'VBP'), ('out', 'IN'), ('of', 'IN'), ('conscious', 'JJ'), ('awareness', 'NN'), ('and', 'CC'), ('influence', 'NN')] 

{'DT': 0, 'NNS': 1, 'CC': 2, 'IN': 4, 'VBN': 7, 'VBP': 8, 'JJ': 11, 'NN': 12}

 --------------------
Thus meaning you do not have to think about engaging your immune system to combat germs and diseases and you do not have to instruct your lymph system to cleanse your body as these processes happen naturally without thinking. 

TAGS: [('Thus', 'RB'), ('meaning', 'VBG'), ('you', 'PRP'), ('do', 'VBP'), ('not', 'RB'), ('have', 'VB'), ('to', 'TO'), ('think', 'VB'), ('about', 'IN'), ('engaging', 'VBG'), ('your', 'PRP$'), ('immune', 'NN'), ('system', 'NN'), ('to', 'TO'), ('combat', 'VB'), ('germs', 'NNS'), ('and', 'CC'), ('diseases', 'NNS'), ('and', 'CC'), ('you', 'PRP'), ('do', 'VBP'), ('not', 'RB'), ('have', 'VB'), ('to', 'TO'), ('instruct', 'VB'), ('your', 'PRP$'), ('lymph', 'NN'), ('system', 'NN'), ('to', 'TO'), ('cleanse', 'VB'), ('your', 'PRP$'), ('body', 'NN'), ('as', 'IN'), ('these', 'DT'), ('processes', 'NNS'), ('happen', 'VBP'), ('naturally', 'RB'), ('without', 'IN'), ('thinking', 'NN')] 

{'RB': 0, 'VBG': 1, 'PRP': 2, 'VBP': 3, 'VB': 5, 'TO': 6, 'IN': 8, 'PRP$': 10, 'NN': 11, 'NNS': 15, 'CC': 16, 'DT': 33}

 --------------------
In healthy people, the aforementioned systems, when functioning normally are far removed from any thought processes. 

TAGS: [('In', 'IN'), ('healthy', 'JJ'), ('people', 'NNS'), ('the', 'DT'), ('aforementioned', 'JJ'), ('systems', 'NNS'), ('when', 'WRB'), ('functioning', 'VBG'), ('normally', 'RB'), ('are', 'VBP'), ('far', 'RB'), ('removed', 'VBN'), ('from', 'IN'), ('any', 'DT'), ('thought', 'JJ'), ('processes', 'NNS')] 

{'IN': 0, 'JJ': 1, 'NNS': 2, 'DT': 3, 'WRB': 6, 'VBG': 7, 'RB': 8, 'VBP': 9, 'VBN': 11}

 --------------------
When there is a problem in the body, the unconscious mind sends a signal via the nervous system. 

TAGS: [('When', 'WRB'), ('there', 'EX'), ('is', 'VBZ'), ('a', 'DT'), ('problem', 'NN'), ('in', 'IN'), ('the', 'DT'), ('body', 'NN'), ('the', 'DT'), ('unconscious', 'JJ'), ('mind', 'NN'), ('sends', 'VBZ'), ('a', 'DT'), ('signal', 'JJ'), ('via', 'IN'), ('the', 'DT'), ('nervous', 'JJ'), ('system', 'NN')] 

{'WRB': 0, 'EX': 1, 'VBZ': 2, 'DT': 3, 'NN': 4, 'IN': 5, 'JJ': 9}

 Question: What is problem?

 --------------------
The signal is frequently experienced as pain or some other physical response. 

TAGS: [('The', 'DT'), ('signal', 'NN'), ('is', 'VBZ'), ('frequently', 'RB'), ('experienced', 'VBN'), ('as', 'IN'), ('pain', 'NN'), ('or', 'CC'), ('some', 'DT'), ('other', 'JJ'), ('physical', 'JJ'), ('response', 'NN')] 

{'DT': 0, 'NN': 1, 'VBZ': 2, 'RB': 3, 'VBN': 4, 'IN': 5, 'CC': 7, 'JJ': 9}

 Question: What is signal?

 --------------------
A simple example is if you ate a very heavy lunch excessively, your digestive system will signal through indigestion that it is struggling to break down the food you have consumed. 

TAGS: [('A', 'DT'), ('simple', 'JJ'), ('example', 'NN'), ('is', 'VBZ'), ('if', 'IN'), ('you', 'PRP'), ('ate', 'VBP'), ('a', 'DT'), ('very', 'RB'), ('heavy', 'JJ'), ('lunch', 'NN'), ('excessively', 'RB'), ('your', 'PRP$'), ('digestive', 'JJ'), ('system', 'NN'), ('will', 'MD'), ('signal', 'VB'), ('through', 'IN'), ('indigestion', 'NN'), ('that', 'IN'), ('it', 'PRP'), ('is', 'VBZ'), ('struggling', 'VBG'), ('to', 'TO'), ('break', 'VB'), ('down', 'RP'), ('the', 'DT'), ('food', 'NN'), ('you', 'PRP'), ('have', 'VBP'), ('consumed', 'VBN')] 

{'DT': 0, 'JJ': 1, 'NN': 2, 'VBZ': 3, 'IN': 4, 'PRP': 5, 'VBP': 6, 'RB': 8, 'PRP$': 12, 'MD': 15, 'VB': 16, 'VBG': 22, 'TO': 23, 'RP': 25, 'VBN': 30}

 Question: What is you struggling?

 --------------------
Chemical shifts in our body through stress and also diet, could create an ulcer, that ulcer is the unconscious mind’s way of saying ‘change something’. 

TAGS: [('Chemical', 'NNP'), ('shifts', 'NNS'), ('in', 'IN'), ('our', 'PRP$'), ('body', 'NN'), ('through', 'IN'), ('stress', 'NN'), ('and', 'CC'), ('also', 'RB'), ('diet', 'JJ'), ('could', 'MD'), ('create', 'VB'), ('an', 'DT'), ('ulcer', 'NN'), ('that', 'DT'), ('ulcer', 'NN'), ('is', 'VBZ'), ('the', 'DT'), ('unconscious', 'JJ'), ('mind', 'NN'), ('’', 'NNP'), ('s', 'VBZ'), ('way', 'NN'), ('of', 'IN'), ('saying', 'VBG'), ('‘', 'NNP'), ('change', 'NN'), ('something', 'NN'), ('’', 'NN')] 

{'NNP': 0, 'NNS': 1, 'IN': 2, 'PRP$': 3, 'NN': 4, 'CC': 7, 'RB': 8, 'JJ': 9, 'MD': 10, 'VB': 11, 'DT': 12, 'VBZ': 16, 'VBG': 24}

 Question: What is Chemical saying?

 --------------------
The above logic can be followed through to more serious issues such as cancer or heart disease. 

TAGS: [('The', 'DT'), ('above', 'JJ'), ('logic', 'NN'), ('can', 'MD'), ('be', 'VB'), ('followed', 'VBN'), ('through', 'IN'), ('to', 'TO'), ('more', 'RBR'), ('serious', 'JJ'), ('issues', 'NNS'), ('such', 'JJ'), ('as', 'IN'), ('cancer', 'NN'), ('or', 'CC'), ('heart', 'NN'), ('disease', 'NN')] 

{'DT': 0, 'JJ': 1, 'NN': 2, 'MD': 3, 'VB': 4, 'VBN': 5, 'IN': 6, 'TO': 7, 'RBR': 8, 'NNS': 10, 'CC': 14}

 --------------------
With cancer the immune system is having a challenge in combating the acceleration of cancerous cells in the body and a tumour is the signal of the shift. 

TAGS: [('With', 'IN'), ('cancer', 'NN'), ('the', 'DT'), ('immune', 'NN'), ('system', 'NN'), ('is', 'VBZ'), ('having', 'VBG'), ('a', 'DT'), ('challenge', 'NN'), ('in', 'IN'), ('combating', 'VBG'), ('the', 'DT'), ('acceleration', 'NN'), ('of', 'IN'), ('cancerous', 'JJ'), ('cells', 'NNS'), ('in', 'IN'), ('the', 'DT'), ('body', 'NN'), ('and', 'CC'), ('a', 'DT'), ('tumour', 'NN'), ('is', 'VBZ'), ('the', 'DT'), ('signal', 'NN'), ('of', 'IN'), ('the', 'DT'), ('shift', 'NN')] 

{'IN': 0, 'NN': 1, 'DT': 2, 'VBZ': 5, 'VBG': 6, 'JJ': 14, 'NNS': 15, 'CC': 19}

 Question: What is cancer having?

 --------------------
With heart disease, what we consume is an influence and also how we live in relation to stress which has an enormous influence on our heart. 

TAGS: [('With', 'IN'), ('heart', 'NN'), ('disease', 'NN'), ('what', 'WP'), ('we', 'PRP'), ('consume', 'VBP'), ('is', 'VBZ'), ('an', 'DT'), ('influence', 'NN'), ('and', 'CC'), ('also', 'RB'), ('how', 'WRB'), ('we', 'PRP'), ('live', 'VBP'), ('in', 'IN'), ('relation', 'NN'), ('to', 'TO'), ('stress', 'NN'), ('which', 'WDT'), ('has', 'VBZ'), ('an', 'DT'), ('enormous', 'JJ'), ('influence', 'NN'), ('on', 'IN'), ('our', 'PRP$'), ('heart', 'NN')] 

{'IN': 0, 'NN': 1, 'WP': 3, 'PRP': 4, 'VBP': 5, 'VBZ': 6, 'DT': 7, 'CC': 9, 'RB': 10, 'WRB': 11, 'TO': 16, 'WDT': 18, 'JJ': 21, 'PRP$': 24}

 --------------------
In 1970s, the co–creators of Neuro Linguistic Programming, Grinder and Bandler created a series of operational assumptions for the practice NLP. 

TAGS: [('In', 'IN'), ('1970s', 'CD'), ('the', 'DT'), ('co–creators', 'NNS'), ('of', 'IN'), ('Neuro', 'NNP'), ('Linguistic', 'NNP'), ('Programming', 'NNP'), ('Grinder', 'NNP'), ('and', 'CC'), ('Bandler', 'NNP'), ('created', 'VBD'), ('a', 'DT'), ('series', 'NN'), ('of', 'IN'), ('operational', 'JJ'), ('assumptions', 'NNS'), ('for', 'IN'), ('the', 'DT'), ('practice', 'NN'), ('NLP', 'NNP')] 

{'IN': 0, 'CD': 1, 'DT': 2, 'NNS': 3, 'NNP': 5, 'CC': 9, 'VBD': 11, 'NN': 13, 'JJ': 15}

 --------------------
One of these assumptions was ‘the mind and body are a linked system’. 

TAGS: [('One', 'CD'), ('of', 'IN'), ('these', 'DT'), ('assumptions', 'NNS'), ('was', 'VBD'), ('‘', 'VBN'), ('the', 'DT'), ('mind', 'NN'), ('and', 'CC'), ('body', 'NN'), ('are', 'VBP'), ('a', 'DT'), ('linked', 'JJ'), ('system', 'NN'), ('’', 'NNP')] 

{'CD': 0, 'IN': 1, 'DT': 2, 'NNS': 3, 'VBD': 4, 'VBN': 5, 'NN': 7, 'CC': 8, 'VBP': 10, 'JJ': 12, 'NNP': 14}

 --------------------
Today most medical practitioners and psychologists would openly accept the mind body connection, however the hypothesis was a lot more radical in 1972. 

TAGS: [('Today', 'NN'), ('most', 'RBS'), ('medical', 'JJ'), ('practitioners', 'NNS'), ('and', 'CC'), ('psychologists', 'NNS'), ('would', 'MD'), ('openly', 'RB'), ('accept', 'VB'), ('the', 'DT'), ('mind', 'NN'), ('body', 'NN'), ('connection', 'NN'), ('however', 'RB'), ('the', 'DT'), ('hypothesis', 'NN'), ('was', 'VBD'), ('a', 'DT'), ('lot', 'NN'), ('more', 'JJR'), ('radical', 'JJ'), ('in', 'IN'), ('1972', 'CD')] 

{'NN': 0, 'RBS': 1, 'JJ': 2, 'NNS': 3, 'CC': 4, 'MD': 6, 'RB': 7, 'VB': 8, 'DT': 9, 'VBD': 16, 'JJR': 19, 'IN': 21, 'CD': 22}

 --------------------
In New Code NLP we take the mind body presupposition a stage further and consider the mind and body as part of the same system, with every thought (mind) being expressed in the body and every feeling (body) being expressed in the mind. 

TAGS: [('In', 'IN'), ('New', 'NNP'), ('Code', 'NNP'), ('NLP', 'NNP'), ('we', 'PRP'), ('take', 'VBP'), ('the', 'DT'), ('mind', 'NN'), ('body', 'NN'), ('presupposition', 'NN'), ('a', 'DT'), ('stage', 'NN'), ('further', 'RBR'), ('and', 'CC'), ('consider', 'VB'), ('the', 'DT'), ('mind', 'NN'), ('and', 'CC'), ('body', 'NN'), ('as', 'IN'), ('part', 'NN'), ('of', 'IN'), ('the', 'DT'), ('same', 'JJ'), ('system', 'NN'), ('with', 'IN'), ('every', 'DT'), ('thought', 'NN'), ('mind', 'NN'), ('being', 'VBG'), ('expressed', 'VBN'), ('in', 'IN'), ('the', 'DT'), ('body', 'NN'), ('and', 'CC'), ('every', 'DT'), ('feeling', 'NN'), ('body', 'NN'), ('being', 'VBG'), ('expressed', 'VBN'), ('in', 'IN'), ('the', 'DT'), ('mind', 'NN')] 

{'IN': 0, 'NNP': 1, 'PRP': 4, 'VBP': 5, 'DT': 6, 'NN': 7, 'RBR': 12, 'CC': 13, 'VB': 14, 'JJ': 23, 'VBG': 29, 'VBN': 30}

 --------------------
These are not linked systems they are different processes within the same system. 

TAGS: [('These', 'DT'), ('are', 'VBP'), ('not', 'RB'), ('linked', 'VBN'), ('systems', 'NNS'), ('they', 'PRP'), ('are', 'VBP'), ('different', 'JJ'), ('processes', 'NNS'), ('within', 'IN'), ('the', 'DT'), ('same', 'JJ'), ('system', 'NN')] 

{'DT': 0, 'VBP': 1, 'RB': 2, 'VBN': 3, 'NNS': 4, 'PRP': 5, 'JJ': 7, 'IN': 9, 'NN': 12}

 --------------------
This ideology of the mind and body as one body-mind, promotes the rapid healing that occurs within the Neuro Hypnotic Healing processes. 

TAGS: [('This', 'DT'), ('ideology', 'NN'), ('of', 'IN'), ('the', 'DT'), ('mind', 'NN'), ('and', 'CC'), ('body', 'NN'), ('as', 'IN'), ('one', 'CD'), ('body-mind', 'NN'), ('promotes', 'VBZ'), ('the', 'DT'), ('rapid', 'JJ'), ('healing', 'NN'), ('that', 'WDT'), ('occurs', 'VBZ'), ('within', 'IN'), ('the', 'DT'), ('Neuro', 'NNP'), ('Hypnotic', 'NNP'), ('Healing', 'NNP'), ('processes', 'NNS')] 

{'DT': 0, 'NN': 1, 'IN': 2, 'CC': 5, 'CD': 8, 'VBZ': 10, 'JJ': 12, 'WDT': 14, 'NNP': 18, 'NNS': 21}

 Question: What promotes Neuro?

 --------------------
For the last 3 centuries, thinking has been promoted as superior to feeling and sensing. 

TAGS: [('For', 'IN'), ('the', 'DT'), ('last', 'JJ'), ('3', 'CD'), ('centuries', 'NNS'), ('thinking', 'NN'), ('has', 'VBZ'), ('been', 'VBN'), ('promoted', 'VBN'), ('as', 'IN'), ('superior', 'JJ'), ('to', 'TO'), ('feeling', 'VBG'), ('and', 'CC'), ('sensing', 'VBG')] 

{'IN': 0, 'DT': 1, 'JJ': 2, 'CD': 3, 'NNS': 4, 'NN': 5, 'VBZ': 6, 'VBN': 7, 'TO': 11, 'VBG': 12, 'CC': 13}

 Question: What has thinking feeling?

 --------------------
Thinking is unique to human beings, and we can even manage to have diverse conversations with ourselves on conflicting topics, convincing ourselves of a position on a life topic and completely change our minds the following day. 

TAGS: [('Thinking', 'NN'), ('is', 'VBZ'), ('unique', 'JJ'), ('to', 'TO'), ('human', 'JJ'), ('beings', 'NNS'), ('and', 'CC'), ('we', 'PRP'), ('can', 'MD'), ('even', 'RB'), ('manage', 'VB'), ('to', 'TO'), ('have', 'VB'), ('diverse', 'JJ'), ('conversations', 'NNS'), ('with', 'IN'), ('ourselves', 'NNS'), ('on', 'IN'), ('conflicting', 'VBG'), ('topics', 'NNS'), ('convincing', 'VBG'), ('ourselves', 'NNS'), ('of', 'IN'), ('a', 'DT'), ('position', 'NN'), ('on', 'IN'), ('a', 'DT'), ('life', 'NN'), ('topic', 'NN'), ('and', 'CC'), ('completely', 'RB'), ('change', 'VB'), ('our', 'PRP$'), ('minds', 'NNS'), ('the', 'DT'), ('following', 'JJ'), ('day', 'NN')] 

{'NN': 0, 'VBZ': 1, 'JJ': 2, 'TO': 3, 'NNS': 5, 'CC': 6, 'PRP': 7, 'MD': 8, 'RB': 9, 'VB': 10, 'IN': 15, 'VBG': 18, 'DT': 23, 'PRP$': 32}

 Question: What is we conflicting?

 --------------------
The expression ‘I think therefore I am’ (Rene Descartes) positioned the logic of thinking (not always logical) over the processes of ‘being’. 

TAGS: [('The', 'DT'), ('expression', 'NN'), ('‘', 'NN'), ('I', 'PRP'), ('think', 'VBP'), ('therefore', 'RB'), ('I', 'PRP'), ('am', 'VBP'), ('’', 'JJ'), ('Rene', 'NNP'), ('Descartes', 'NNP'), ('positioned', 'VBD'), ('the', 'DT'), ('logic', 'NN'), ('of', 'IN'), ('thinking', 'NN'), ('not', 'RB'), ('always', 'RB'), ('logical', 'JJ'), ('over', 'IN'), ('the', 'DT'), ('processes', 'NNS'), ('of', 'IN'), ('‘', 'NN'), ('being', 'VBG'), ('’', 'NNP')] 

{'DT': 0, 'NN': 1, 'PRP': 3, 'VBP': 4, 'RB': 5, 'JJ': 8, 'NNP': 9, 'VBD': 11, 'IN': 14, 'NNS': 21, 'VBG': 24}

 --------------------
Being is the processes inherent in the body. 

TAGS: [('Being', 'NNP'), ('is', 'VBZ'), ('the', 'DT'), ('processes', 'NNS'), ('inherent', 'JJ'), ('in', 'IN'), ('the', 'DT'), ('body', 'NN')] 

{'NNP': 0, 'VBZ': 1, 'DT': 2, 'NNS': 3, 'JJ': 4, 'IN': 5, 'NN': 7}

 Question: What is Being?

 --------------------
Thinking is the key feature of the conscious mind, and the conscious mind seeks to dominate with its thinking. 

TAGS: [('Thinking', 'NN'), ('is', 'VBZ'), ('the', 'DT'), ('key', 'JJ'), ('feature', 'NN'), ('of', 'IN'), ('the', 'DT'), ('conscious', 'JJ'), ('mind', 'NN'), ('and', 'CC'), ('the', 'DT'), ('conscious', 'JJ'), ('mind', 'NN'), ('seeks', 'VBZ'), ('to', 'TO'), ('dominate', 'VB'), ('with', 'IN'), ('its', 'PRP$'), ('thinking', 'NN')] 

{'NN': 0, 'VBZ': 1, 'DT': 2, 'JJ': 3, 'IN': 5, 'CC': 9, 'TO': 14, 'VB': 15, 'PRP$': 17}

 Question: What is Thinking?

 --------------------
When a person lets their conscious attention be dominant, it could be said the person is living in their head. 

TAGS: [('When', 'WRB'), ('a', 'DT'), ('person', 'NN'), ('lets', 'VBZ'), ('their', 'PRP$'), ('conscious', 'JJ'), ('attention', 'NN'), ('be', 'VB'), ('dominant', 'JJ'), ('it', 'PRP'), ('could', 'MD'), ('be', 'VB'), ('said', 'VBD'), ('the', 'DT'), ('person', 'NN'), ('is', 'VBZ'), ('living', 'VBG'), ('in', 'IN'), ('their', 'PRP$'), ('head', 'NN')] 

{'WRB': 0, 'DT': 1, 'NN': 2, 'VBZ': 3, 'PRP$': 4, 'JJ': 5, 'VB': 7, 'PRP': 9, 'MD': 10, 'VBD': 12, 'VBG': 16, 'IN': 17}

 Question: What lets it living?

 --------------------
Such head dominance leads to disconnection from body awareness so apart for perhaps the primal needs of going to the toilet, hunger and sex, the person living in their head has little awareness of their body. 

TAGS: [('Such', 'JJ'), ('head', 'NN'), ('dominance', 'NN'), ('leads', 'VBZ'), ('to', 'TO'), ('disconnection', 'NN'), ('from', 'IN'), ('body', 'NN'), ('awareness', 'NN'), ('so', 'IN'), ('apart', 'RB'), ('for', 'IN'), ('perhaps', 'RB'), ('the', 'DT'), ('primal', 'JJ'), ('needs', 'NNS'), ('of', 'IN'), ('going', 'VBG'), ('to', 'TO'), ('the', 'DT'), ('toilet', 'NN'), ('hunger', 'NN'), ('and', 'CC'), ('sex', 'NN'), ('the', 'DT'), ('person', 'NN'), ('living', 'VBG'), ('in', 'IN'), ('their', 'PRP$'), ('head', 'NN'), ('has', 'VBZ'), ('little', 'JJ'), ('awareness', 'NN'), ('of', 'IN'), ('their', 'PRP$'), ('body', 'NN')] 

{'JJ': 0, 'NN': 1, 'VBZ': 3, 'TO': 4, 'IN': 6, 'RB': 10, 'DT': 13, 'NNS': 15, 'VBG': 17, 'CC': 22, 'PRP$': 28}

 Question: What leads head going?

 --------------------
Such dissociation is unhealthy on the basis without body awareness the messages from the unconscious mind signalling a change in the system are not experienced and a health issue develops unnoticed. 

TAGS: [('Such', 'JJ'), ('dissociation', 'NN'), ('is', 'VBZ'), ('unhealthy', 'JJ'), ('on', 'IN'), ('the', 'DT'), ('basis', 'NN'), ('without', 'IN'), ('body', 'NN'), ('awareness', 'IN'), ('the', 'DT'), ('messages', 'NNS'), ('from', 'IN'), ('the', 'DT'), ('unconscious', 'JJ'), ('mind', 'NN'), ('signalling', 'VBG'), ('a', 'DT'), ('change', 'NN'), ('in', 'IN'), ('the', 'DT'), ('system', 'NN'), ('are', 'VBP'), ('not', 'RB'), ('experienced', 'VBN'), ('and', 'CC'), ('a', 'DT'), ('health', 'NN'), ('issue', 'NN'), ('develops', 'VBZ'), ('unnoticed', 'JJ')] 

{'JJ': 0, 'NN': 1, 'VBZ': 2, 'IN': 4, 'DT': 5, 'NNS': 11, 'VBG': 16, 'VBP': 22, 'RB': 23, 'VBN': 24, 'CC': 25}

 Question: What is dissociation signalling?

 --------------------
One of the key premises of the Neuro Hypnotic Healing system is to a partnership between the conscious and unconscious mind. 

TAGS: [('One', 'CD'), ('of', 'IN'), ('the', 'DT'), ('key', 'JJ'), ('premises', 'NNS'), ('of', 'IN'), ('the', 'DT'), ('Neuro', 'NNP'), ('Hypnotic', 'NNP'), ('Healing', 'NNP'), ('system', 'NN'), ('is', 'VBZ'), ('to', 'TO'), ('a', 'DT'), ('partnership', 'NN'), ('between', 'IN'), ('the', 'DT'), ('conscious', 'JJ'), ('and', 'CC'), ('unconscious', 'JJ'), ('mind', 'NN')] 

{'CD': 0, 'IN': 1, 'DT': 2, 'JJ': 3, 'NNS': 4, 'NNP': 7, 'NN': 10, 'VBZ': 11, 'TO': 12, 'CC': 18}

 Question: What is Neuro?

 --------------------
In this partnership the conscious mind can take the role of organiser (rather than controller) and the unconscious can take the role of provider, providing the necessary resources for healing and well being. 

TAGS: [('In', 'IN'), ('this', 'DT'), ('partnership', 'NN'), ('the', 'DT'), ('conscious', 'JJ'), ('mind', 'NN'), ('can', 'MD'), ('take', 'VB'), ('the', 'DT'), ('role', 'NN'), ('of', 'IN'), ('organiser', 'NN'), ('rather', 'RB'), ('than', 'IN'), ('controller', 'NN'), ('and', 'CC'), ('the', 'DT'), ('unconscious', 'JJ'), ('can', 'MD'), ('take', 'VB'), ('the', 'DT'), ('role', 'NN'), ('of', 'IN'), ('provider', 'NN'), ('providing', 'VBG'), ('the', 'DT'), ('necessary', 'JJ'), ('resources', 'NNS'), ('for', 'IN'), ('healing', 'NN'), ('and', 'CC'), ('well', 'RB'), ('being', 'VBG')] 

{'IN': 0, 'DT': 1, 'NN': 2, 'JJ': 4, 'MD': 6, 'VB': 7, 'RB': 12, 'CC': 15, 'VBG': 24, 'NNS': 27}

 --------------------
This may seem simplistic but such a partnership between the conscious and unconscious puts the mind and body as one is the route for a healthy life. 

TAGS: [('This', 'DT'), ('may', 'MD'), ('seem', 'VB'), ('simplistic', 'JJ'), ('but', 'CC'), ('such', 'PDT'), ('a', 'DT'), ('partnership', 'NN'), ('between', 'IN'), ('the', 'DT'), ('conscious', 'JJ'), ('and', 'CC'), ('unconscious', 'JJ'), ('puts', 'VBZ'), ('the', 'DT'), ('mind', 'NN'), ('and', 'CC'), ('body', 'NN'), ('as', 'IN'), ('one', 'CD'), ('is', 'VBZ'), ('the', 'DT'), ('route', 'NN'), ('for', 'IN'), ('a', 'DT'), ('healthy', 'JJ'), ('life', 'NN')] 

{'DT': 0, 'MD': 1, 'VB': 2, 'JJ': 3, 'CC': 4, 'PDT': 5, 'NN': 7, 'IN': 8, 'VBZ': 13, 'CD': 19}

 Question: What puts partnership?

 --------------------
About The Author
Michael Carroll is the founder and course director of the NLP Academy and co-founder with John Grinder and Carmen Bostic St Clair of the International Trainers Academy of NLP. 

TAGS: [('About', 'IN'), ('The', 'DT'), ('Author', 'NNP'), ('Michael', 'NNP'), ('Carroll', 'NNP'), ('is', 'VBZ'), ('the', 'DT'), ('founder', 'NN'), ('and', 'CC'), ('course', 'NN'), ('director', 'NN'), ('of', 'IN'), ('the', 'DT'), ('NLP', 'NNP'), ('Academy', 'NNP'), ('and', 'CC'), ('co-founder', 'NN'), ('with', 'IN'), ('John', 'NNP'), ('Grinder', 'NNP'), ('and', 'CC'), ('Carmen', 'NNP'), ('Bostic', 'NNP'), ('St', 'NNP'), ('Clair', 'NNP'), ('of', 'IN'), ('the', 'DT'), ('International', 'NNP'), ('Trainers', 'NNP'), ('Academy', 'NNP'), ('of', 'IN'), ('NLP', 'NNP')] 

{'IN': 0, 'DT': 1, 'NNP': 2, 'VBZ': 5, 'NN': 7, 'CC': 8}

 Question: What is Author?

 --------------------
He is the only NLP Master Trainer in the world certified by John Grinder and Carmen Bostic St Clair and he works closely with them in developing and delivering high quality NLP training. 

TAGS: [('He', 'PRP'), ('is', 'VBZ'), ('the', 'DT'), ('only', 'JJ'), ('NLP', 'NNP'), ('Master', 'NNP'), ('Trainer', 'NNP'), ('in', 'IN'), ('the', 'DT'), ('world', 'NN'), ('certified', 'VBN'), ('by', 'IN'), ('John', 'NNP'), ('Grinder', 'NNP'), ('and', 'CC'), ('Carmen', 'NNP'), ('Bostic', 'NNP'), ('St', 'NNP'), ('Clair', 'NNP'), ('and', 'CC'), ('he', 'PRP'), ('works', 'VBZ'), ('closely', 'RB'), ('with', 'IN'), ('them', 'PRP'), ('in', 'IN'), ('developing', 'VBG'), ('and', 'CC'), ('delivering', 'VBG'), ('high', 'JJ'), ('quality', 'NN'), ('NLP', 'NNP'), ('training', 'NN')] 

{'PRP': 0, 'VBZ': 1, 'DT': 2, 'JJ': 3, 'NNP': 4, 'IN': 7, 'NN': 9, 'VBN': 10, 'CC': 14, 'RB': 22, 'VBG': 26}

 Question: What is NLP developing?
```

## How to improve this program.

* This program generates questions starting with 'What'. We can add rule for generating questions containing 'How', 'Where', 'When', 'Which' etc.

* We can use a dataset of text and questions along with machine learning to ask better questions. 

* Further, we can add complex semantic rules for creating long and complex questions.

* We can use pre-tagged bag of words to improve part-of-speech tags.

## Reference 

[Alphabetical list of part-of-speech tags used in the Penn Treebank Project](http://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html)

[Automatic Factual Question Generation from Text](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.208.5602&rep=rep1&type=pdf)

[TextBlob: Simplified Text Processing](http://textblob.readthedocs.io/en/dev/index.html)

[Automatic Question Generation from Paragraph](http://www.ijaerd.com/papers/finished_papers/Automatic%20Question%20Generation%20from%20Paragraph-IJAERDV03I1213514.pdf)

[K2Q: Generating Natural Language Questions from Keywords with User Refinements](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/37566.pdf)

[Literature Review of Automatic Question Generation Systems](https://pdfs.semanticscholar.org/fee0/1067ea9ce9ac1d85d3fd84c3b7f363a3826b.pdf)
